import copy
import random
from utils import get_uniform, get_norm
import numpy as np


def get_random_complex(n, l, a, b):
    N = 2 * n  # рекомедуемое количество точек
    X_0 = np.array(get_uniform(a, b))
    C_0 = [X_0]
    for i in range(1, N):
        phi = get_ones_vector(n)
        phi_norm = get_norm(phi)
        new_X = X_0 + l * (phi / phi_norm)
        C_0.append(np.array(new_X))
        while not in_S(C_0[i], a, b):
            C_0 = complex_compression(C_0.copy(), i)
    return C_0


# отражение k вершины комплекса C_r
def complex_reflection(C_r, k, alpha=1.3):
    N = len(C_r)
    C_next = C_r.copy()
    center = get_complex_gravity_center(C_next.copy())
    C_next[k] = center + alpha * (center - C_r[k])
    return C_next


def get_complex_gravity_center(C_r):
    N = len(C_r)
    sum = np.copy(C_r[0])
    for i in range(1, N):
        sum += C_r[i]
    return sum / N


def complex_compression(C_r, k, beta=0.5):
    C_new = C_r.copy()
    center = get_complex_gravity_center(C_new.copy())
    C_new[k] = center + beta * (C_r[k] - center)
    return C_new


def get_ones_vector(n):
    phi = [random.uniform(-1, 1) for _ in range(n)]
    return np.array(phi)


def in_S(X, a, b):
    for i in range(len(X)):
        if not a[i] <= X[i] <= b[i]:
            return False
    return True


def get_max_complex_f(f, C):
    max_val = f(C[0])
    max_num = 0
    for i in range(1, len(C)):
        val = f(C[i])
        if val > max_val:
            max_val = val
            max_num = i
    return max_num, max_val


def get_min_complex_f(f, C):
    min_val = f(C[0])
    min_num = 0
    for i in range(1, len(C)):
        val = f(C[i])
        if val < min_val:
            min_val = val
            min_num = i
    return min_num, min_val


def check_end(C, eps):
    n = len(C)
    max_norm = get_norm(C[0]-C[n-1])
    for i in range(n-1):
        norm = get_norm(C[i] - C[i+1])
        if norm > max_norm:
            max_norm = norm
    if max_norm < eps:
        return True
    return False


def modified_complex(f, a, b, eps=0.01, _l=1, max_iter=100):
    n = len(a)
    C_0 = get_random_complex(n, _l, a, b)
    N = len(C_0)
    r = 0
    C = C_0
    while True:
        max_num, max_val = get_max_complex_f(f, C)
        C = complex_reflection(C, max_num)
        new_val = f(C[max_num])
        is_S = in_S(C[max_num], a, b)

        if new_val < max_val and not in_S:
            for i in range(len(a)):
                if C[max_num][i] < a[i]:
                    C[max_num][i] = a[i]
                elif C[max_num][i] > b[i]:
                    C[max_num][i] = b[i]
        else:
            if not in_S and new_val > max_val:
                while not in_S(C[max_num], a, b):
                    C = complex_compression(C.copy(), max_num)
            if check_end(C, eps):
                print('Закончили как молодчики')
                num, val = get_min_complex_f(f, C)
                return C[num], val
        r += 1
        if r > max_iter:
            print('Закончили из-за итераций')
            answer = get_min_complex_f(f, C)
            return answer

    pass
