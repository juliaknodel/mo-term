import random
import math


def get_uniform(a, b):
    x_uniform = []
    for i in range(len(a)):
        new_num = random.uniform(a[i], b[i])
        x_uniform.append(new_num)
    return x_uniform


def get_diff_norm(x, y):
    diff = [(x[i] - y[i])**2 for i in range(len(x))]
    return sum(diff)


def get_norm(x):
    return math.sqrt(sum(x**2))