import math
import numpy as np


# функция  содним глобальным жкстремумом
def f(x):
    return x[0]**2 + x[1]**2 - 1


a = np.array([-1, -1])
b = np.array([1, 1])
f_ans = np.array([0, 0])


# функция с 4 эстремумами при ограничениях (2 из них глоб. - x<0, 2 локальных - x>0)
def f_2(x):
    return x[0]**2 + 2*math.sin(2*x[0]) + math.cos(x[1])


a_2 = np.array([-3, -3])
b_2 = np.array([3, 3])
