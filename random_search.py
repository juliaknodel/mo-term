from utils import get_uniform


def random_search(f, a, b, steps):
    x_min = [float('inf') for _ in range(len(a))]
    f_min = float('inf')
    for i in range(steps+1):
        x = get_uniform(a, b)
        if f(x) < f_min:
            x_min = x
            f_min = f(x_min)
    return x_min
