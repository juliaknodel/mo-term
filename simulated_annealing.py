import random
import math
from utils import get_uniform


# Input:
# f - минимизируемая функция (любая)
# a, b - из R^n нижние и верхние границы условий (линейных)
def simulated_annealing(f, a, b, steps, p_0=0.99, _lambda=0.95):
    x = get_uniform(a, b)
    T_0 = -1/(math.log10(p_0))
    T = T_0
    x_min = x
    for i in range(1, steps+1):
        x_new = get_uniform(a, b)
        p = min(1, math.exp(-(abs(f(x)-f(x_new)))/T))
        if f(x_new) < f(x) or p > random.uniform(0, 1):
            if f(x_new) < f(x):
                x_min = x_new
            x = x_new
        T = T*_lambda
    return x_min
