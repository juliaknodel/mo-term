import numpy as np

from simulated_annealing import simulated_annealing
from random_search import random_search
from modified_complex import modified_complex
from utils import get_diff_norm
from problem import f, a, b, f_ans, f_2, a_2, b_2

steps_list = [10, 50, 100, 1000]
print('Имитация отжига:\n')
for steps in steps_list:
    x = simulated_annealing(f, a, b, steps)
    print('\nЧисло шагов: ', steps)
    print('Результат: ', x)
    print('Норма разности с истинным ответом: ', get_diff_norm(x, f_ans))

print('\n\nСлучайный поиск:\n')
for steps in steps_list:
    x = random_search(f, a, b, steps)
    print('\nЧисло шагов: ', steps)
    print('Результат: ', x)
    print('Норма разности с истинным ответом: ', get_diff_norm(x, f_ans))

print('\n\nИмитация отжига ф2:\n')
for steps in steps_list:
    x = simulated_annealing(f_2, a_2, b_2, steps)
    print('\nЧисло шагов: ', steps)
    print('Результат: ', x)

print('\n\nСлучайный поиск ф2:\n')
for steps in steps_list:
    x = random_search(f_2, a_2, b_2, steps)
    print('\nЧисло шагов: ', steps)
    print('Результат: ', x)

# print(modified_complex(f, a, b))


epsilons = [0.1, 0.01, 0.001]
print("\n\n\nМодифицированный метод комплексов")
for eps in epsilons:
    x, val = modified_complex(f, a, b, eps=eps)
    print('\nТочность: ', eps)
    print('Результат: ', x)
    print('Норма разности с истинным ответом: ', get_diff_norm(x, f_ans))




